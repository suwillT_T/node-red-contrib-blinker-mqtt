module.exports = function(RED) { // RED  可以对node-red 进行访问
	var client = {};
	var fromDevice = '';
	RED.nodes.registerType("Blinker-IN", class {
		constructor(config) {
			const node = this
			RED.nodes.createNode(node, config)
			const BlinkerNodeConfig = RED.nodes.getNode(config.blinker)
			// console.log('blinkerConfig blinker-mqtt.js:',BlinkerNodeConfig)
			let Blinker_key = BlinkerNodeConfig.SecretKey;
			var flowContext = this.context().flow;
			// flowContext.set(Blinker_key, {
			// 	type: BlinkerNodeConfig.DeviceType
			// })
			// console.log('读取数值：', flowContext.get(Blinker_key).action || '{"pState": "false"}')
			var mqtt = require('mqtt');
			console.log("Blinker_key(BlinkerNodeConfig):", Blinker_key)
			function getBlinkerDeviceInfo(auth, callback) {
				let host = 'https://iot.diandeng.tech';
				let url = '';
				if (BlinkerNodeConfig.DeviceType != 'other') {
					url = '/api/v1/user/device/diy/auth?authKey=' + auth + '&miType=' + BlinkerNodeConfig.DeviceType +
						'&version=1.2.2';
				} else {
					url = '/api/v1/user/device/diy/auth?authKey=' + auth;
				}
				let https = require('https');
				https.get(host + url, function(res) {
					let datas = [];
					let size = 0;
					res.on('data', function(data) {
						datas.push(data);
						size += data.length;
					})
					res.on('end', function(data) {
						let mProto = {};
						let buff = Buffer.concat(datas, size);
						var data = JSON.parse(buff);
						if (data['detail'] == 'device not found') {
							console.log('Please make sure you have put in the right AuthKey!');
							// node.warn('没有找到设备,AuthKey错误?')
							node.status({
								text: `没有找到设备,AuthKey错误?`,
								fill: 'red',
								shape: 'ring'
							})
						} else {
							node.status({
								text: `获取到设备信息:${BlinkerNodeConfig.name}(${BlinkerNodeConfig.SecretKey})`,
								fill: 'blue',
								shape: 'ring'
							})
							let dd = data['detail'];
							// console.log('device found');
							let deviceName = dd.deviceName;
							let iotId = dd.iotId;
							let iotToken = dd.iotToken;
							let productKey = dd.productKey;
							let uuid = dd.uuid;
							let broker = dd.broker;

							if (broker == 'aliyun') {
								mProto._protocol = 'mqtt://'
								mProto._host = 'public.iot-as-mqtt.cn-shanghai.aliyuncs.com'
								mProto._port = 1883;
								mProto._subtopic = '/' + productKey + '/' + deviceName + '/r';
								mProto._pubtopic = '/' + productKey + '/' + deviceName + '/s';
								mProto._clientId = deviceName;
								mProto._username = iotId;
							} else if (broker == 'qcloud') {
								mProto._protocol = 'mqtt://'
								mProto._host = 'iotcloud-mqtt.gz.tencentdevices.com'
								mProto._port = 1883;
								mProto._subtopic = productKey + '/' + deviceName + '/r'
								mProto._pubtopic = productKey + '/' + deviceName + '/s'
								mProto._clientId = productKey + deviceName
								mProto._username = mProto._clientId + ';' + iotId
							} else if (broker == 'blinker') {
								mProto._protocol = dd.host.substring(0, dd.host.indexOf("://") + 3);
								mProto._host = dd.host.substring(dd.host.indexOf("://") + 3);
								mProto._port = 1883;
								mProto._subtopic = '/device/' + deviceName + '/r'
								mProto._pubtopic = '/device/' + deviceName + '/s'
								mProto._clientId = deviceName
								mProto._username = iotId
							}
							mProto._deviceName = deviceName
							mProto._password = iotToken
							mProto._uuid = uuid
							flowContext.set(BlinkerNodeConfig.SecretKey, {
								type: BlinkerNodeConfig.DeviceType,
								mqtt: mProto
							})
							node.send({
								payload: "NodeRed<-Blinker->MQTT OK"
							})
							callback(mProto)
						}
					})
				}).on('error', function(err) {
					console.log('Get Device Info Error...' + err);
					node.status({
						text: `获取设备失败:${err}`,
						fill: 'red',
						shape: 'ring'
					})
				})
			}
			getBlinkerDeviceInfo(BlinkerNodeConfig.SecretKey, function(mProto) {
				let mqttProto = flowContext.get(BlinkerNodeConfig.SecretKey).mqtt;
				var options = {
					clientId: mqttProto._clientId,
					username: mqttProto._username,
					password: mqttProto._password,
				}
				client[Blinker_key] = mqtt.connect(mqttProto._protocol + mqttProto._host + ':' + mqttProto._port, options);
				client[Blinker_key].on('connect', function() {
					client[Blinker_key].subscribe(mqttProto._subtopic);
					console.log(Blinker_key + ':|/--------------MQTT:<->Connected!--------------/');
					node.status({
						text: `onLine:${BlinkerNodeConfig.name}(${BlinkerNodeConfig.SecretKey})`,
						fill: 'green',
						shape: 'ring'
					})
				})
				client[Blinker_key].on('message', function(topic, message) {
					let data = message.toString(); // message is Buffer
					// node.send([JSON.parse(data), null])
					console.log('MQTT:<-|', data);
					let recvMessage = JSON.parse(data);
					data = recvMessage['data'];
					if (recvMessage.fromDevice === 'ServerSender' && recvMessage?.data?.from === 'MIOT') {
						//米家设备
						// let queryDevice = JSON.parse(data)
						let messageId = recvMessage.data.messageId;
						if (recvMessage?.data?.get === 'state') {
							console.log('MQTT:<-|米家获取状态(ALL):', JSON.stringify(recvMessage?.data))
							// if (get_msg.data.hasOwnProperty('get') && get_msg.data.get == 'state') {
							// let out_parm = nodeContext.get('MIOT-Device') || '{"pState": "false"}';
							if (BlinkerNodeConfig.autoRes) {
								let out_parm = flowContext.get(BlinkerNodeConfig.SecretKey).action || '{"pState": "false"}';
								console.log('MQTT:->|自动反馈米家获取状态(ALL):', out_parm)
								// 小米状态查询包
								client[Blinker_key].publish(mqttProto._pubtopic, JSON.stringify({
									"fromDevice": mqttProto._deviceName,
									"toDevice": "ServerReceiver",
									"data": JSON.parse(out_parm),
									"deviceType": "vAssistant"
								}));
							}
							node.send({
								payload: recvMessage?.data,
								SecretKey: Blinker_key
							})
							// console.log('////心跳包发完了')
						} else if (recvMessage?.data?.get === 'state') {
							console.log('MQTT:<-|米家获取状态:', JSON.stringify(recvMessage?.data))
							node.send({
								payload: recvMessage?.data,
								SecretKey: Blinker_key
							})
							if (BlinkerNodeConfig.autoRes) {
								let respParam = flowContext.get(BlinkerNodeConfig.SecretKey).action || { pState: "true" };
								respParam.messageId = messageId;
								let resp = {
									"data": respParam,
									"fromDevice": mqttProto._deviceName, 
									"toDevice": "ServerReceiver",
									"deviceType": "vAssistant"
								}
								console.log('MQTT:->|自动反馈米家获取状态:', JSON.stringify(resp));
								client[Blinker_key].publish(mqttProto._pubtopic, JSON.stringify(resp));
							}
						} else if (recvMessage?.data?.set !== undefined) {
							console.log('MQTT:<-|米家操控指令:', JSON.stringify(recvMessage?.data))
							// {"set":{"pState":true,"num":"1"}}
							// {"fromDevice":"ServerSender","data":{"set":{"pState":"false","pstate":"off"},"from":"MIOT","messageId":"66801543ffd0796b"}}
							// {"fromDevice":"ServerSender","data":{"set":{"pState":"true","pstate":"on"},"from":"MIOT","messageId":"66801532f6154549"}}
							// console.log(JSON.parse(data).set)
							//原样怼回去了
							if (BlinkerNodeConfig.autoRes) {
								let in_parm = recvMessage?.data?.set;
								flowContext.set(BlinkerNodeConfig.SecretKey, {
									action: JSON.stringify(in_parm),
									type: BlinkerNodeConfig.DeviceType,
									mqtt: mqttProto
								});
								in_parm.messageId = messageId;
								// { "fromDevice": "00B9730CLLXCD5A1GOZ1FXKE", "toDevice": "ServerReceiver", "data": {"pState":"on","messageId":"668011339431f328"}, "deviceType": "vAssistant"}
								let resp = {
									"fromDevice": mqttProto._deviceName,
									"toDevice": "ServerReceiver",
									"data": in_parm,
									"deviceType": "vAssistant"
								}
								console.log('MQTT:->|自动反馈米家操控指令:', JSON.stringify(resp));
								client[Blinker_key].publish(mqttProto._pubtopic, JSON.stringify(resp));
							}
							//原样怼回去了
							node.send({
								payload: recvMessage?.data,
								SecretKey: Blinker_key
							})
						} else {
							console.log('MQTT:<-|其他情况:', data)
						}
					} else {
						//非米家设备
						if (recvMessage?.data?.get === 'state') {
							//APP心跳包
							console.log('MQTT:->|非米家设备(APP心跳包):', JSON.stringify(recvMessage?.data));
							client[Blinker_key].publish(mqttProto._pubtopic, JSON.stringify({
								"data": {
									"state": "online",
									"timer": "000",
									"version": "0.1.0"
								},
								"fromDevice": mqttProto._deviceName,
								"toDevice": mqttProto._uuid,
								"deviceType": "OwnApp"
							}));
						} else {
							//非心跳包抛向前台
							console.log('MQTT:<-|非米家设备:', JSON.stringify(recvMessage?.data))
							node.send({
								payload: recvMessage?.data,
								SecretKey: Blinker_key
							})
						}
					}
				})
				client[Blinker_key].on('error', function(err) {
					console.log(err);
					node.status({
						text: `MQTT错误:${err}`,
						fill: 'yellow',
						shape: 'dot'
					})
				})
			})
			node.on('close', function(removed, done) {
				for (var mqtt_client in client) {
					client[mqtt_client].end();
				}
				done()
			});
		}
	});
	RED.nodes.registerType('Blinker-OUT', class {
		constructor(config) {
			const node = this
			RED.nodes.createNode(node, config)
			var flowContext = this.context().flow;
			node.on('input', data => {
				if (data.hasOwnProperty('send')&&data.hasOwnProperty('SecretKey')) {
					let Blinker_key=data.SecretKey
					console.log('Blinker_key(Blinker-OUT-inputFunction):', Blinker_key)
					let mqtt = flowContext.get(Blinker_key).mqtt;
					// console.log(mqtt._pubtopic)
					node.status({
						text: JSON.stringify(data.payload),
						fill: 'green',
						shape: 'ring'
					})
					let send_msg = {
						"data": data.payload,
						"fromDevice": mqtt._deviceName,
						"toDevice": fromDevice == 'MIOT' ? "ServerReceiver" : mqtt._uuid,
						"deviceType": fromDevice == 'MIOT' ? "vAssistant" : "OwnApp"
					}
					node.send({
						payload: send_msg
					})
					if (send_msg.deviceType === 'vAssistant') {
						client[Blinker_key].publish(mqtt._pubtopic, JSON.stringify(send_msg));
					} else {
						client[Blinker_key].publish(mqtt._pubtopic, JSON.stringify(send_msg));
					}
					
					console.log('MQTT:->|input:', JSON.stringify(send_msg));
				} else {
					node.status({
						text: '因msg.send或msg.SecretKey缺失\n发布失败',
						fill: 'red',
						shape: 'ring'
					})
					node.send(data)
				}
			})
		}
	})
}
